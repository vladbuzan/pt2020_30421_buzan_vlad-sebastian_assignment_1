import model.Polynomial;
import org.junit.Test;

import static controller.Utils.parsePolynomial;
import static org.junit.Assert.assertEquals;


public class PolynomialCalculatorTest {
    @Test
    public void testAdd() {
        try{
        Polynomial polynomial1 = parsePolynomial("2x^2 + x + 5");
        Polynomial polynomial2 = parsePolynomial("x^2 + 3x + 6");
        polynomial1.add(polynomial2);
        Polynomial result = parsePolynomial("3x^2 + 4x + 11");
        assertEquals(result.toString(), polynomial1.toString());
    } catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    @Test
    public void testSubtract() {
        try {
            Polynomial polynomial1 = parsePolynomial("2x^2 + x + 5");
            Polynomial polynomial2 = parsePolynomial("x^2 + x + 4");
            polynomial1.subtract(polynomial2);
            Polynomial result = parsePolynomial("x^2 + 1");
            assertEquals(result.toString(), polynomial1.toString());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    @Test
    public void testDifferentiate() {
        try {
            Polynomial polynomial = parsePolynomial("3x^3");
            Polynomial result = parsePolynomial("9x^2");
            polynomial.differentiate();
            assertEquals(polynomial.toString(), result.toString());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    @Test
    public void testIntegrate() {
        try {
            Polynomial polynomial = parsePolynomial("3x^2");
            Polynomial result = parsePolynomial("x^3");
            polynomial.integrate();
            assertEquals(result.toString(), polynomial.toString());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    @Test
    public void testMultiply() {
        try {
            Polynomial polynomial1 = parsePolynomial("2x");
            Polynomial polynomial2 = parsePolynomial("x^2 + 5");
            Polynomial result = parsePolynomial("2x^3 + 10x");
            polynomial1.multiply(polynomial2);
            assertEquals(result.toString(), polynomial1.toString());
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    @Test
    public void testDivide() {
        try{
            Polynomial polynomial1 = parsePolynomial("2x^4-3x^3-15x^2+32x-12");
            Polynomial polynomial2 = parsePolynomial("x^2-4x-12");
            Polynomial rest = polynomial1.divide(polynomial2);
            assertEquals(rest.toString(), parsePolynomial("208x+336").toString());
            assertEquals(polynomial1.toString(), parsePolynomial("2x^2+5x+29").toString());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
